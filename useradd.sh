#!/bin/bash

# Creating Directories
declare -a dirname=("publico" "adm" "ven" "sec")
for dir in "${dirname[@]}"
  do
    if [ ! -d "$dir" ]
    then
      mkdir "/$dir"
    else
      echo "Directory already exists."
    fi
  done

# Creating Groups
declare -a groups=("GRP_ADM" "GRP_VEN" "GRP_SEC")
for grp in "${groups[@]}"
  do
    if [ ! -d "$grp" ]
    then
      groupadd $grp
    else
      echo "Group already exists."
    fi
  done

# Creating Users
declare -a username=("Carlos" "Maria" "Joao" "Debora" "Sebastiana" "Roberto" "Josefina" "Amanda" "Rogerio")
for user in "${username[@]}"
  do
    if [ ! -d "$user" ]
    then
      useradd $user -c "$user" -s /bin/bash -m -p $(openssl passwd -crypt Senha123)
    else
      echo "User already exists."
    fi
  done

echo  
echo USUÁRIOS CRIADOS
echo  
tail -n 9 /etc/passwd
echo  

#Adding Users to Group
usermod -G GRP_ADM Carlos
usermod -G GRP_ADM Maria
usermod -G GRP_ADM Joao 
usermod -G GRP_VEN Debora
usermod -G GRP_VEN Sebastiana
usermod -G GRP_VEN Roberto
usermod -G GRP_SEC Josefina
usermod -G GRP_SEC Amanda
usermod -G GRP_SEC Rogerio

#Changing Ownershp
chown root:root /publico
chown root:GRP_ADM /adm
chown root:GRP_VEN /ven
chown root:GRP_SEC /sec

#Changing Permissions
chmod 777 /publico
chmod 770 /adm
chmod 770 /ven
chmod 770 /sec
echo  
echo  
echo GRUPOS COM USUÁRIOS  
echo  
cat /etc/group | grep GRP
echo  

